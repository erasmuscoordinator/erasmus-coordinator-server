package com.edu.agh.erasmus.coordinator.university.offer;

import lombok.Data;

import java.util.List;

@Data
public class EmbeddedOffers {
    private final List<Offer> agreements;
}
