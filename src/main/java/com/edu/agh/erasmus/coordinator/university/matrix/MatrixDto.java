package com.edu.agh.erasmus.coordinator.university.matrix;

import com.edu.agh.erasmus.coordinator.university.application.dto.ApplicationApplierDto;
import com.edu.agh.erasmus.coordinator.university.offer.Offer;
import lombok.Data;

import java.util.List;

@Data
class MatrixDto {
    private final Offer offer;
    private final List<ApplicationApplierDto> applications;
}
