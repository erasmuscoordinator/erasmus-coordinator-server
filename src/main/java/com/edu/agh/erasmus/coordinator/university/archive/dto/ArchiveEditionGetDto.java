package com.edu.agh.erasmus.coordinator.university.archive.dto;

import com.edu.agh.erasmus.coordinator.university.application.dto.ApplicationsGetDto;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.Collection;

@Data
@Builder
@RequiredArgsConstructor
public class ArchiveEditionGetDto {
    private final Collection<ApplicationsGetDto> applications;
    private final String edition;
}
