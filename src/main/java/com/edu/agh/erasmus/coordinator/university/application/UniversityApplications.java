package com.edu.agh.erasmus.coordinator.university.application;

import com.edu.agh.erasmus.coordinator.university.user.User;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Wither;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Document
@Data
@Builder
@Wither
public class UniversityApplications {
    @Id private final ObjectId id;
    private final List<UniversityApplication> applications;
    private final ObjectId applierId;
    private final User applierData;
    private final LocalDateTime created;
    private final boolean approvedByDwz;
}
