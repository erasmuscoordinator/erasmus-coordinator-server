package com.edu.agh.erasmus.coordinator.infrastructure.exception;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
class ErasmusExceptionDto {
    private final String description;
    private final String status;

    static ErasmusExceptionDto of(ErasmusException ex){
        return new ErasmusExceptionDto(ex.getDescription(), ex.getStatus().toString());
    }
}
