package com.edu.agh.erasmus.coordinator.university.application.dto;

import com.edu.agh.erasmus.coordinator.university.application.UniversityApplication;
import com.edu.agh.erasmus.coordinator.university.application.UniversityApplications;
import com.edu.agh.erasmus.coordinator.university.offer.Offer;
import com.edu.agh.erasmus.coordinator.university.user.ApplicationUserDto;
import com.edu.agh.erasmus.coordinator.university.user.User;
import com.google.common.collect.Streams;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static com.edu.agh.erasmus.coordinator.university.utils.ObjectIdConverter.toObjectId;
import static java.util.stream.Collectors.toList;

@Data
@Builder
public class ApplicationsSaveDto {
    private final List<ApplicationSaveDto> applications;
    private final String applierId;
    private final ApplicationUserDto applierData;
    private final LocalDateTime created;
    private final boolean approvedByDwz;

    public UniversityApplications asUniversityApplications(List<Offer> offers) {
        return UniversityApplications.builder()
                .applications(processApplications(applierData.asUser(), offers))
                .applierId(toObjectId(applierId))
                .applierData(applierData.asUser())
                .created(LocalDateTime.now())
                .approvedByDwz(false)
                .build();
    }

    private List<UniversityApplication> processApplications(User user, List<Offer> offers) {
        List<ApplicationSaveDto> indexedApplications = addPreferencesToApplications();
        return Streams.mapWithIndex(indexedApplications.stream(), (it, index) -> it.asUniversityApplication(user, offers.get((int) index)))
                .collect(toList());
    }

    private List<ApplicationSaveDto> addPreferencesToApplications() {
        return Streams.mapWithIndex(applications.stream(), (it, index) -> it.withPreference((int) index + 1)).collect(Collectors.toList());
    }
}
