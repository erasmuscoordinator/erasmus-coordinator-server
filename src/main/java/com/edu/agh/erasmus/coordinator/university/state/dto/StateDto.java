package com.edu.agh.erasmus.coordinator.university.state.dto;

import com.edu.agh.erasmus.coordinator.university.state.EnrollmentState;
import com.edu.agh.erasmus.coordinator.university.state.State;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StateDto {
    private final EnrollmentState enrollmentState;

    public static StateDto of(State state) {
        return builder()
                .enrollmentState(state.getEnrollmentState())
                .build();
    }
}
