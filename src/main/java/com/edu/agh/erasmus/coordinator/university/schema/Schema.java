package com.edu.agh.erasmus.coordinator.university.schema;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Wither;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

@Data
@Builder
@Wither
public class Schema {
    @Id private final ObjectId schemaId;
    private final String modifiedByUsername;
    private final String content;
    private final String modificationReason;
    private final LocalDateTime modified;
}
