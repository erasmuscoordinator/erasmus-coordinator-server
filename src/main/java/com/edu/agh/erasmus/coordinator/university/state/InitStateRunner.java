package com.edu.agh.erasmus.coordinator.university.state;

import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class InitStateRunner implements ApplicationRunner {
    private final StateRepository stateRepository;

    public void run(ApplicationArguments args) {
        if (stateRepository.count() == 0) {
            stateRepository.save(new State(new ObjectId(), EnrollmentState.BEFORE_ENROLLMENT));
        }
    }

}
