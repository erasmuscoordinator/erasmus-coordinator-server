package com.edu.agh.erasmus.coordinator.university.offer;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Wither;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

@Data
@Wither
@Builder
public class Agreement {
    @Id private final ObjectId id;
    private final String country;
    private final String code;
    private final String universityName;
    private final String startYear;
    private final String endYear;
    private final String department;
    private final String coordinator;

    //data not from scrapper
    private final String coordinatorId;       //todo add this in agreements (discuss flow)
    private final String departmentCoordinatorId;    //todo same
    private final String departmentCoordinator;
    private final String duration;
    private final String vacancies;

    Offer toOffer(){
        return Offer.builder()
                .id(id.toString())
                .country(country)
                .code(code)
                .universityName(universityName)
                .startYear(startYear)
                .endYear(endYear)
                .department(department)
                .coordinator(coordinator)
                .coordinator(coordinatorId)
                .departmentCoordinatorId(departmentCoordinatorId)
                .departmentCoordinator(departmentCoordinator)
                .duration(duration)
                .vacancies(vacancies)
                .build();
    }
}
