package com.edu.agh.erasmus.coordinator.university.utils;

public final class DepartmentsCheckUtil {
    public static boolean checkDepartmentsEquality(String offerDepartment, String userDepartment) {
        return offerDepartment.equals(userDepartment);
    }
}
