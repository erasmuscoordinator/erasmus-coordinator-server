package com.edu.agh.erasmus.coordinator.university.archive;

import com.edu.agh.erasmus.coordinator.university.application.UniversityApplications;
import lombok.Data;

import java.util.Collection;

@Data
public class ArchiveEdition {
    private final Collection<UniversityApplications> applications;
    private final String edition;
}
