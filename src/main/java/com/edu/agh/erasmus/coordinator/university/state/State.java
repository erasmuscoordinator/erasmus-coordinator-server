package com.edu.agh.erasmus.coordinator.university.state;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Wither;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

@Data
@Builder
@Wither
public class State {
    @Id private final ObjectId id;
    private final EnrollmentState enrollmentState;
}
