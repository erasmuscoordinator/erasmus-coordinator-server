package com.edu.agh.erasmus.coordinator.university.application.dto;

import com.edu.agh.erasmus.coordinator.university.user.ApplicationUserDto;
import lombok.Data;

@Data
public class ApplicationApplierDto {
    private final ApplicationGetDto application;
    private final ApplicationUserDto user;
}
