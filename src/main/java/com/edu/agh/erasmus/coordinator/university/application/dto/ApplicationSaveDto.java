package com.edu.agh.erasmus.coordinator.university.application.dto;

import com.edu.agh.erasmus.coordinator.university.application.UniversityApplication;
import com.edu.agh.erasmus.coordinator.university.offer.Offer;
import com.edu.agh.erasmus.coordinator.university.user.User;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Wither;

import static com.edu.agh.erasmus.coordinator.university.application.ApplicationStatus.UNKNOWN;
import static com.edu.agh.erasmus.coordinator.university.utils.DepartmentsCheckUtil.checkDepartmentsEquality;
import static com.edu.agh.erasmus.coordinator.university.utils.ObjectIdConverter.toObjectId;

@Data
@Builder
@Wither
public class ApplicationSaveDto {
    private final String universityOfferId;
    private final String status;
    private final int preference;
    private final String duration;
    private final String semester;

    UniversityApplication asUniversityApplication(User user, Offer offer) {
        return UniversityApplication.builder()
                .universityOfferId(toObjectId(universityOfferId))
                .offerData(offer)
                .status(UNKNOWN)
                .preference(preference)
                .duration(duration)
                .semester(semester)
                .isApplierFromTheSameDepartmentAsOffer(checkDepartmentsEquality(offer.getDepartment(), user.getDepartment()))
                .build();
    }
}
