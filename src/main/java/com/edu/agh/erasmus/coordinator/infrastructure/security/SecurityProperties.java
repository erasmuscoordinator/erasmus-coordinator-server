package com.edu.agh.erasmus.coordinator.infrastructure.security;

public class SecurityProperties {
    public static final String AUTH_HEADER = "Authorization";
    static final String TOKEN_PREFIX = "Bearer ";
}
