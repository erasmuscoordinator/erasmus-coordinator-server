package com.edu.agh.erasmus.coordinator.university.archive;

import com.edu.agh.erasmus.coordinator.university.archive.dto.ArchiveEditionGetDto;
import com.edu.agh.erasmus.coordinator.university.archive.dto.EditionDto;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.edu.agh.erasmus.coordinator.university.utils.Authorities.ONLY_ADMINISTRATOR;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/archive")
public class ArchiveController {
    private final ArchiveService archiveService;

    @GetMapping("{edition}")
    @ApiOperation(value = "Return specified archival edition")
    @PreAuthorize(ONLY_ADMINISTRATOR)
    ResponseEntity<ArchiveEditionGetDto> getFromArchive(@PathVariable String edition) {
        return new ResponseEntity<>(archiveService.getArchivalEdition(edition), HttpStatus.OK);
    }

    @PostMapping
    @ApiOperation(value = "Places current applications in archival edition")
    @PreAuthorize(ONLY_ADMINISTRATOR)
    ResponseEntity saveInArchive(@RequestBody EditionDto archiveEdition) {
        archiveService.archive(archiveEdition);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
