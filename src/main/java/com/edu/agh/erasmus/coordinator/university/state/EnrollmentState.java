package com.edu.agh.erasmus.coordinator.university.state;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum EnrollmentState {
    BEFORE_ENROLLMENT(1),
    ENROLLMENT(2),
    QUALIFICATIONS(3),
    DWZ_APPROVALS(4);

    private int stateCode;

    public static EnrollmentState getNextState(EnrollmentState enrollmentState) {
        for (EnrollmentState nextStateCandidate : EnrollmentState.values()) {
            if (nextStateCandidate.stateCode - 1 == enrollmentState.stateCode) {
                return nextStateCandidate;
            }
        }
        return enrollmentState;
    }

    public static EnrollmentState getPreviousState(EnrollmentState enrollmentState) {
        for (EnrollmentState previousStateCandidate : EnrollmentState.values()) {
            if (previousStateCandidate.stateCode + 1 == enrollmentState.stateCode) {
                return previousStateCandidate;
            }
        }
        return enrollmentState;
    }
}
