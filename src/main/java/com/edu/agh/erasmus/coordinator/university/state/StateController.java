package com.edu.agh.erasmus.coordinator.university.state;

import com.edu.agh.erasmus.coordinator.university.state.dto.StateDto;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.edu.agh.erasmus.coordinator.university.utils.Authorities.ONLY_ADMINISTRATOR;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/state/")
public class StateController {
    private final StateService stateService;

    @GetMapping("current")
    @ApiOperation(value = "Get current state of enrollment process")
    ResponseEntity<StateDto> getCurrentState() {
        return new ResponseEntity<>(stateService.getCurrentState(), HttpStatus.OK);
    }

    @PutMapping
    @ApiOperation(value = "Set state of enrollment process")
    @PreAuthorize(ONLY_ADMINISTRATOR)
    ResponseEntity<StateDto> setCurrentState(@RequestBody StateDto newState) {
        return new ResponseEntity<>(stateService.setCurrentState(newState), HttpStatus.OK);
    }

    @PutMapping("increment")
    @ApiOperation(value = "Increment current state of enrollment process")
    @PreAuthorize(ONLY_ADMINISTRATOR)
    ResponseEntity incrementCurrentState() {
        return new ResponseEntity<>(stateService.incrementState(), HttpStatus.OK);
    }

    @PutMapping("decrement")
    @ApiOperation(value = "Decrement current state of enrollment process")
    @PreAuthorize(ONLY_ADMINISTRATOR)
    ResponseEntity decrementCurrentState() {
        return new ResponseEntity<>(stateService.decrementState(), HttpStatus.OK);
    }

}
