package com.edu.agh.erasmus.coordinator.university.utils;

public class Authorities {
    private final static String ADMINISTRATOR = "ADMINISTRATOR";
    private final static String COORDINATOR = "COORDINATOR";

    public final static String ONLY_ADMINISTRATOR = "hasAuthority('" + ADMINISTRATOR + "')";
    public final static String ADMINISTRATOR_OR_COORDINATOR = "hasAnyAuthority('" + ADMINISTRATOR + "', '" + COORDINATOR + "')";
}
