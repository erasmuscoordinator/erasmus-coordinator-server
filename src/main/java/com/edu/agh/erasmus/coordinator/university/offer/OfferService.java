package com.edu.agh.erasmus.coordinator.university.offer;

import com.codahale.metrics.annotation.Timed;
import com.edu.agh.erasmus.coordinator.university.utils.ObjectIdConverter;
import com.github.benmanes.caffeine.cache.Cache;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static com.edu.agh.erasmus.coordinator.infrastructure.security.SecurityProperties.AUTH_HEADER;

@Service
public class OfferService {

    private static final String DEFAULT_DEPARTMENT = "WIEIT";

    private final RestTemplate offersRestTemplate;
    private final Cache<String, Offer> agreementsCache;
    private final String offersHost;
    private final AgreementRepository agreementRepository;

    public OfferService(RestTemplate offersRestTemplate,
                        Cache<String, Offer> agreementsCache,
                        @Value("${offers.host}") String offersHost, AgreementRepository agreementRepository) {
        this.offersRestTemplate = offersRestTemplate;
        this.agreementsCache = agreementsCache;
        this.offersHost = offersHost;
        this.agreementRepository = agreementRepository;
    }

//    TEMPORARY GET OFFER FROM DATABASE
//    @Timed
//    public Offer getOfferById(String agreementId, String token) {
//        return agreementRepository.findOneById(ObjectIdConverter.toObjectId(agreementId)).toOffer();
//    }

    @Timed
    public Offer getOfferById(String agreementId, String token) {
        return agreementsCache.get(agreementId, id -> getOffer("/agreements/" + agreementId, token));
    }

    @Timed
    public List<Offer> getOffersForCoordinator(String coordinatorName, String token) {
        return getOffers("/agreements/search/coordinators?coordinator=" + coordinatorName + "&sort=country&size=200", token);
    }

    @Timed
    public List<Offer> getOffersForDepartment(String token) {
        return getOffersForDepartment(DEFAULT_DEPARTMENT, token);
    }

    private List<Offer> getOffersForDepartment(String department, String token) {
        return getOffers("/agreements/search/departments?department=" + department + "&sort=country&size=200", token); //todo sorting here only?
    }

    private List<Offer> getOffers(String endpoint, String token) {
        HttpEntity<String> entity = createEntity(token);
        return offersRestTemplate
                .exchange(offersHost + endpoint, HttpMethod.GET, entity, Offers.class)
                .getBody().get_embedded().getAgreements();
    }

    private Offer getOffer(String endpoint, String token) {
        HttpEntity<String> entity = createEntity(token);
        return offersRestTemplate
                .exchange(offersHost + endpoint, HttpMethod.GET, entity, Offer.class)
                .getBody();
    }

    private HttpEntity<String> createEntity(String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(AUTH_HEADER, token);
        return new HttpEntity<>("parameters", headers);
    }
}
