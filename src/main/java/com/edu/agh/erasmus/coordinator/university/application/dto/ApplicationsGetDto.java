package com.edu.agh.erasmus.coordinator.university.application.dto;

import com.edu.agh.erasmus.coordinator.university.application.UniversityApplications;
import com.edu.agh.erasmus.coordinator.university.user.ApplicationUserDto;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Data
@Builder
public class ApplicationsGetDto {
    private final String id;
    private final List<ApplicationGetDto> applications;
    private final String applierId;
    private final ApplicationUserDto applierData;
    private final LocalDateTime created;
    private final boolean approvedByDwz;

    public static ApplicationsGetDto of(UniversityApplications application) {
        return builder()
                .id(application.getId().toHexString())
                .applications(application.getApplications().stream().map(ApplicationGetDto::of).collect(toList()))
                .applierId(application.getApplierId().toHexString())
                .applierData(ApplicationUserDto.of(application.getApplierData()))
                .created(application.getCreated())
                .approvedByDwz(application.isApprovedByDwz())
                .build();
    }
}
