package com.edu.agh.erasmus.coordinator.university.offer;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AgreementRepository extends MongoRepository<Agreement, ObjectId> {

    Agreement findOneById(ObjectId id);
}
