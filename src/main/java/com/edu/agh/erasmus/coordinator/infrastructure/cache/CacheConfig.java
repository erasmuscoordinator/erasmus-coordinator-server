package com.edu.agh.erasmus.coordinator.infrastructure.cache;

import com.codahale.metrics.MetricRegistry;
import com.edu.agh.erasmus.coordinator.university.offer.Offer;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CacheConfig {

    @Bean
    public Cache<String, Offer> agreementsCache(MetricRegistry metricRegistry, @Value("${cache.agreements}") String cacheSpec) {
        return Caffeine.from(cacheSpec)
                .recordStats(() -> new MetricsStatsCounter(metricRegistry, "cache.agreements"))
                .build();
    }
}
