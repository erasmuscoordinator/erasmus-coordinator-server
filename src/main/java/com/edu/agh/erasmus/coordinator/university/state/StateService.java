package com.edu.agh.erasmus.coordinator.university.state;

import com.edu.agh.erasmus.coordinator.university.state.dto.StateDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

import static com.edu.agh.erasmus.coordinator.university.state.EnrollmentState.BEFORE_ENROLLMENT;

@RequiredArgsConstructor
@Service
public class StateService {
    private final StateRepository stateRepository;

    StateDto getCurrentState() {
        return StateDto.of(getState());
    }

    StateDto incrementState() {
        State stateToIncrement = getState();
        stateToIncrement = stateToIncrement.withEnrollmentState(EnrollmentState.getNextState(stateToIncrement.getEnrollmentState()));
        stateRepository.save(stateToIncrement);
        return StateDto.of(stateToIncrement);
    }

    StateDto decrementState() {
        State stateToDecrement = getState();
        stateToDecrement = stateToDecrement.withEnrollmentState(EnrollmentState.getPreviousState(stateToDecrement.getEnrollmentState()));
        stateRepository.save(stateToDecrement);
        return StateDto.of(stateToDecrement);
    }

    StateDto setCurrentState(StateDto state) {
        State currentState = getState();
        State newState = currentState.withEnrollmentState(state.getEnrollmentState());
        State savedState = stateRepository.save(newState);
        return StateDto.of(savedState);
    }

    @PostConstruct
    private void initStateOnDb() {
        if (undefinedState()) {
            State initStateToSave = State.builder().enrollmentState(BEFORE_ENROLLMENT).build();
            stateRepository.save(initStateToSave);
        }
    }

    private boolean undefinedState() {
        return stateRepository.count() == 0
                || getState() == null
                || getState().getEnrollmentState() == null;
    }

    private State getState() {
        return stateRepository.findAll().get(0);
    }
}
