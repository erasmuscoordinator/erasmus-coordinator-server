package com.edu.agh.erasmus.coordinator.university.schema.dto;

import com.edu.agh.erasmus.coordinator.university.schema.Schema;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class SchemaSaveDto {
    private final String modifiedUserName;
    private final String content;
    private final String modificationReason;

    public Schema asSchema() {
        return Schema.builder()
                .modifiedByUsername(modifiedUserName)
                .modificationReason(modificationReason)
                .modified(LocalDateTime.now())
                .content(content)
                .build();
    }
}
