package com.edu.agh.erasmus.coordinator.university.user;

import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static com.edu.agh.erasmus.coordinator.university.utils.HtmlEncoder.htmlEncode;

@Data
@Builder
public class User { //todo divide this into student, coordinator, etc with accounts
    @Id private final ObjectId id;

    private final String email;
    private final String password;

    private final String firstName;
    private final String lastName;
    private final String university;
    private final String department;
    private final String faculty;
    private final StudiesDegree studiesDegree;

    //STUDENT ONLY
    private final String address;
    private final String phone;
    private final double gpa;
    private final List<Language> language;
    private final ErasmusParticipation erasmusParticipation;
    private final String completedSemester;
    private final boolean hasSocialScholarship;
    private final boolean hasPowerScholarship;
    private final boolean hasDisabilities;
    private final boolean isUserOnFirstSemesterSecondDegree;

    private final String indexNumber;
    private final String yearOfStudy;
    private final UniversityRole universityRole;

    public enum StudiesDegree {
        BACHELOR, MASTERS, SUPPLEMENTARY, POST_GRADUATE, DOCTORATE
    }

    public enum UniversityRole {
        STUDENT, COORDINATOR, ADMINISTRATOR
    }
}

@Data
@Builder
class Language {
    private final String name;
    private final String examLevel;
    private final String grade;
    private final String certificateType;

    public static List<Language> of(Collection<Language> languages) {
        if (languages == null) {
            return null;
        }
        return languages.stream()
                .map(Language::of)
                .collect(Collectors.toList());
    }

    private static Language of(Language language) {
        return builder()
                .name(htmlEncode(language.getName()))
                .examLevel(htmlEncode(language.getExamLevel()))
                .grade(htmlEncode(language.getGrade()))
                .certificateType(htmlEncode(language.getCertificateType()))
                .build();
    }
}

@Data
@Builder
class ErasmusParticipation {
    private final String yearOfStudy;
    private final String studiesDegree;
    private final String duration;

    public static ErasmusParticipation of(ErasmusParticipation participation) {
        if (participation == null) {
            return null;
        }
        return builder()
                .yearOfStudy(htmlEncode(participation.getYearOfStudy()))
                .studiesDegree(htmlEncode(participation.getStudiesDegree()))
                .duration(htmlEncode(participation.getDuration()))
                .build();
    }
}
