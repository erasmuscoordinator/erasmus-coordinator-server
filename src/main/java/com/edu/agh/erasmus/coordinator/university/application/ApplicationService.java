package com.edu.agh.erasmus.coordinator.university.application;

import com.edu.agh.erasmus.coordinator.infrastructure.exception.ErasmusException;
import com.edu.agh.erasmus.coordinator.university.application.dto.ApplicationApprovalDto;
import com.edu.agh.erasmus.coordinator.university.application.dto.ApplicationGetDto;
import com.edu.agh.erasmus.coordinator.university.application.dto.ApplicationSaveDto;
import com.edu.agh.erasmus.coordinator.university.application.dto.ApplicationUpdateDto;
import com.edu.agh.erasmus.coordinator.university.application.dto.ApplicationsGetDto;
import com.edu.agh.erasmus.coordinator.university.application.dto.ApplicationsSaveDto;
import com.edu.agh.erasmus.coordinator.university.application.dto.ApprovedApplicationsDto;
import com.edu.agh.erasmus.coordinator.university.offer.Offer;
import com.edu.agh.erasmus.coordinator.university.offer.OfferService;
import com.edu.agh.erasmus.coordinator.university.utils.ObjectIdConverter;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.edu.agh.erasmus.coordinator.university.application.ApplicationStatus.APPROVED;
import static com.edu.agh.erasmus.coordinator.university.utils.ObjectIdConverter.toObjectId;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@Service
@RequiredArgsConstructor
class ApplicationService {

    private final static String OWNER_OR_ADMIN = "hasAuthority('ADMINISTRATOR') || returnObject.applierData.email == authentication.name";
    private final ApplicationRepository applicationRepository;
    private final ConcreteApplicationRepository concreteApplicationRepository;
    private final OfferService offerService;

    Set<ApplicationsGetDto> getAllApplications() {
        return applicationRepository.findAll()
                .stream()
                .map(ApplicationsGetDto::of)
                .collect(toSet());
    }

    ApplicationsGetDto getUniversityApplicationById(String id) {
        UniversityApplications application = applicationRepository.findOneById(toObjectId(id))
                .orElseThrow(() -> new ErasmusException("No application found for id: " + id, NOT_FOUND));
        return ApplicationsGetDto.of(application);
    }

    @PostAuthorize(OWNER_OR_ADMIN)
    ApplicationsGetDto getUniversityApplicationByApplierId(String applierId) {
        UniversityApplications application = applicationRepository.findOneByApplierId(toObjectId(applierId))
                .orElseThrow(() -> new ErasmusException("No application found for applier id: " + applierId, NO_CONTENT));
        return ApplicationsGetDto.of(application);
    }

    ApplicationsGetDto addApplication(ApplicationsSaveDto application, String token) {
        Optional<UniversityApplications> possibleApplication = applicationRepository.findOneByApplierId(toObjectId(application.getApplierId()));
        UniversityApplications newApplication = application.asUniversityApplications(getOffers(application, token));
        UniversityApplications applicationToSave = (possibleApplication.isPresent())
                ? newApplication.withId(possibleApplication.get().getId()) : newApplication;
        return ApplicationsGetDto.of(concreteApplicationRepository.save(applicationToSave));
    }

    @PostAuthorize(OWNER_OR_ADMIN)
    ApplicationsGetDto updateApplicationApplierData(ApplicationUpdateDto application) {
        UniversityApplications applicationToUpdate = applicationRepository.findOne(toObjectId(application.getId()));
        UniversityApplications savedApplication = applicationRepository.save(applicationToUpdate.withApplierData(application.getApplierData()));
        return ApplicationsGetDto.of(savedApplication);
    }

    @PostAuthorize(OWNER_OR_ADMIN)
    ApplicationsGetDto updateApplicationOfferData(ApplicationUpdateDto application) {
        UniversityApplications applicationToUpdate = applicationRepository.findOne(toObjectId(application.getId()));
        UniversityApplications savedApplication = applicationRepository.save(
                applicationToUpdate.withApplications(application.getApplications().stream()
                        .map(applicationGetDto -> applicationGetDto.asUniversityApplication(application.getApplierData()))
                        .collect(toList())));
        return ApplicationsGetDto.of(savedApplication);
    }

    @PostAuthorize(OWNER_OR_ADMIN)
    void removeApplication(String id) { //todo why void? return deleted application
        applicationRepository.deleteApplicationById(id);
    }

    void updateApplicationApprovalStatus(ApplicationApprovalDto application) {
        Set<ObjectId> ids = application.getIds().stream().map(ObjectIdConverter::toObjectId).collect(toSet());
        Iterable<UniversityApplications> applicationsToUpdate = applicationRepository.findAll(ids);
        applicationsToUpdate.forEach(it -> applicationRepository.save(it.withApprovedByDwz(application.isApprovedByDwz())));
    }

    List<ApprovedApplicationsDto> getApproved(String department, String coordinatorName, Boolean approvedByDwz) {
        Set<ApplicationsGetDto> applications = approvedByDwz == null ? getAllApplications() : getApplicationsApprovedByDwz(approvedByDwz);
        return filterApprovedApplications(department, coordinatorName, applications);
    }

    private List<Offer> getOffers(ApplicationsSaveDto application, String token) {
        return application.getApplications().stream()
                .map(ApplicationSaveDto::getUniversityOfferId)
                .map(id -> offerService.getOfferById(id, token))
                .collect(Collectors.toList());
    }

    private Set<ApplicationsGetDto> getApplicationsApprovedByDwz(Boolean approvedByDwz) {
        return applicationRepository.findByApprovedByDwz(approvedByDwz)
                .stream()
                .map(ApplicationsGetDto::of)
                .collect(toSet());
    }

    private List<ApprovedApplicationsDto> filterApprovedApplications(String department, String coordinatorName, Set<ApplicationsGetDto> applications) {       //todo get from database
        List<ApprovedApplicationsDto> approved = applications
                .stream()
                .map(it -> new ApprovedApplicationsDto(it.getId(), getOnlyApprovedApplication(it.getApplications()), it.getApplierData()))
                .filter(it -> it.getApplication() != null)
                .sorted(comparing(it -> it.getApplierData().getLastName()))
                .collect(toList());

        if (department != null) {
            approved = approved.stream()
                    .filter(it -> filterDepartments(it, department))
                    .collect(toList());
        }

        if (coordinatorName != null) {
            approved = approved.stream()
                    .filter(it -> filterCoordinators(it, coordinatorName))
                    .collect(toList());
        }
        return approved;
    }

    private boolean filterCoordinators(ApprovedApplicationsDto applications, String coordinatorName) {
        return applications.getApplication().getOfferData().getCoordinator().toLowerCase().contains(coordinatorName.toLowerCase());
    }

    private boolean filterDepartments(ApprovedApplicationsDto applications, String department) {
        if (department.substring(0, 3).equals("non")) {
            return !applications.getApplierData().getDepartment().toLowerCase().equals(department.substring(3).toLowerCase());
        } else {
            return applications.getApplierData().getDepartment().toLowerCase().equals(department.toLowerCase());
        }
    }

    private ApplicationGetDto getOnlyApprovedApplication(List<ApplicationGetDto> applications) {
        return applications.stream().filter(it -> it.getStatus().equals(APPROVED.toString())).findFirst().orElse(null);
    }
}
