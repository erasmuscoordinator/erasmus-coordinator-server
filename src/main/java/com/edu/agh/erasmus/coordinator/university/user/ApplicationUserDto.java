package com.edu.agh.erasmus.coordinator.university.user;

import com.edu.agh.erasmus.coordinator.infrastructure.exception.ErasmusException;
import com.edu.agh.erasmus.coordinator.university.user.User.StudiesDegree;
import com.edu.agh.erasmus.coordinator.university.utils.ObjectIdConverter;
import lombok.Builder;
import lombok.Data;

import java.util.Arrays;
import java.util.List;

import static com.edu.agh.erasmus.coordinator.university.utils.HtmlEncoder.htmlEncode;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Data
@Builder
public class ApplicationUserDto {
    private final String id;
    private final String email;
    private final String firstName;
    private final String lastName;
    private final String university;
    private final String department;
    private final String faculty;
    private final String indexNumber;
    private final String yearOfStudy;
    private final String studiesDegree;
    private final String address;
    private final String phone;
    private final double gpa;
    private final List<Language> language;
    private final ErasmusParticipation erasmusParticipation;
    private final String completedSemester;
    private final boolean hasSocialScholarship;
    private final boolean hasPowerScholarship;
    private final boolean hasDisabilities;
    private final boolean isUserOnFirstSemesterSecondDegree;

    public User asUser() {
        return User.builder()
                .id(ObjectIdConverter.toObjectId(id))
                .email(email)
                .firstName(firstName)
                .lastName(lastName)
                .university(university)
                .department(department)
                .faculty(faculty)
                .indexNumber(indexNumber)
                .yearOfStudy(yearOfStudy)
                .studiesDegree(validateStudiesDegree())
                .address(address)
                .phone(phone)
                .gpa(gpa)
                .language(language)
                .erasmusParticipation(erasmusParticipation)
                .completedSemester(completedSemester)
                .hasSocialScholarship(hasSocialScholarship)
                .hasPowerScholarship(hasPowerScholarship)
                .hasDisabilities(hasDisabilities)
                .isUserOnFirstSemesterSecondDegree(isUserOnFirstSemesterSecondDegree)
                .build();
    }

    private StudiesDegree validateStudiesDegree() {  //todo could be handled by generic method e.g. 'validateEnumValues'
        try {
            return StudiesDegree.valueOf(studiesDegree);
        } catch (IllegalArgumentException e) {
            throw new ErasmusException("Invalid user studies degree, correct values: " + Arrays.toString(StudiesDegree.values()), BAD_REQUEST);
        }
    }

    public static ApplicationUserDto of(User user) {
        return builder()
                .id(user.getId().toHexString())
                .email(htmlEncode(user.getEmail()))
                .firstName(htmlEncode(user.getFirstName()))
                .lastName(htmlEncode(user.getLastName()))
                .university(htmlEncode(user.getUniversity()))
                .department(htmlEncode(user.getDepartment()))
                .faculty(htmlEncode(user.getFaculty()))
                .indexNumber(htmlEncode(user.getIndexNumber()))
                .yearOfStudy(htmlEncode(user.getYearOfStudy()))
                .studiesDegree(String.valueOf(user.getStudiesDegree()))
                .address(htmlEncode(user.getAddress()))
                .phone(htmlEncode(user.getPhone()))
                .gpa((user.getGpa()))
                .language(Language.of(user.getLanguage()))
                .erasmusParticipation(ErasmusParticipation.of(user.getErasmusParticipation()))
                .completedSemester(htmlEncode(user.getCompletedSemester()))
                .hasSocialScholarship(user.isHasSocialScholarship())
                .hasPowerScholarship(user.isHasPowerScholarship())
                .hasDisabilities(user.isHasDisabilities())
                .isUserOnFirstSemesterSecondDegree(user.isUserOnFirstSemesterSecondDegree())
                .build();
    }
}
