package com.edu.agh.erasmus.coordinator.university.application.dto;

import com.edu.agh.erasmus.coordinator.university.user.User;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
public class ApplicationUpdateDto {
    @NotNull private final String id;
    private final User applierData;
    private final List<ApplicationGetDto> applications;
}
