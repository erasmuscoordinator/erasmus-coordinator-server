package com.edu.agh.erasmus.coordinator.university.archive.dto;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@RequiredArgsConstructor
public class EditionDto {
    private final String edition;
}
