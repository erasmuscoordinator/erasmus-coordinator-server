package com.edu.agh.erasmus.coordinator.university.archive;

import com.edu.agh.erasmus.coordinator.infrastructure.exception.ErasmusException;
import com.edu.agh.erasmus.coordinator.university.application.ApplicationRepository;
import com.edu.agh.erasmus.coordinator.university.application.UniversityApplications;
import com.edu.agh.erasmus.coordinator.university.application.dto.ApplicationsGetDto;
import com.edu.agh.erasmus.coordinator.university.archive.dto.ArchiveEditionGetDto;
import com.edu.agh.erasmus.coordinator.university.archive.dto.EditionDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@RequiredArgsConstructor
@Service
public class ArchiveService {
    private final ArchiveRepository archiveRepository;
    private final ApplicationRepository applicationRepository;

    void archive(EditionDto editionDto) {
        validateEdition(editionDto.getEdition());
        List<UniversityApplications> allApplications = applicationRepository.findAll();
        ArchiveEdition archiveEdition = new ArchiveEdition(allApplications, editionDto.getEdition());
        archiveRepository.save(archiveEdition);
        validateArchiveSaving(editionDto, allApplications);
        applicationRepository.deleteAll();
    }

    ArchiveEditionGetDto getArchivalEdition(String edition) {
        ArchiveEdition archivalEdition = archiveRepository.findOneByEdition(edition);
        if (archivalEdition == null) {
            throw new ErasmusException("Edition does not exists: " + edition, BAD_REQUEST);
        }
        List<ApplicationsGetDto> applications = archivalEdition.getApplications().stream()
                .map(ApplicationsGetDto::of)
                .collect(Collectors.toList());

        return new ArchiveEditionGetDto(applications, archivalEdition.getEdition());
    }

    private void validateEdition(String edition) {
        ArchiveEdition possibleEdition = archiveRepository.findOneByEdition(edition);
        if (possibleEdition != null) {
            throw new ErasmusException("Edition already exists: " + edition, BAD_REQUEST);
        }
    }

    private void validateArchiveSaving(EditionDto editionDto, List<UniversityApplications> currentApplications) {
        Collection<UniversityApplications> archiveApplications = archiveRepository.findOneByEdition(editionDto.getEdition()).getApplications();
        if (archiveApplications.size() != currentApplications.size()) {
            throw new ErasmusException("Error saving applications: ", INTERNAL_SERVER_ERROR);
        }
    }
}
