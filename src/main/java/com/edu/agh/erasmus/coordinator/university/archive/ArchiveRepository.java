package com.edu.agh.erasmus.coordinator.university.archive;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ArchiveRepository extends MongoRepository<ArchiveEdition, ObjectId> {
    
    ArchiveEdition findOneByEdition(String edition);
}
