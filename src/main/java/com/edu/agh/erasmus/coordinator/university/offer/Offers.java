package com.edu.agh.erasmus.coordinator.university.offer;

import lombok.Data;

@Data
public class Offers {
    private final EmbeddedOffers _embedded;
}