package com.edu.agh.erasmus.coordinator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@SpringBootApplication
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class ErasmusApp {

	public static void main(String[] args) {
		SpringApplication.run(ErasmusApp.class, args);
	}
}
