package com.edu.agh.erasmus.coordinator.university.application;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface ApplicationRepository extends MongoRepository<UniversityApplications, ObjectId> {
//    Set<UniversityApplications> findByUniversityOfferId(ObjectId offerId);
    Optional<UniversityApplications> findOneById(ObjectId offerId);
    Optional<UniversityApplications> findOneByApplierId(ObjectId offerId);
    Long deleteApplicationById(String id);
    List<UniversityApplications> findByApprovedByDwz(boolean isApproved);
}
