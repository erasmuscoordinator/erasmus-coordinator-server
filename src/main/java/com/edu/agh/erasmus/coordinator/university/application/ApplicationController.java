package com.edu.agh.erasmus.coordinator.university.application;

import com.codahale.metrics.annotation.Timed;
import com.edu.agh.erasmus.coordinator.infrastructure.exception.ErasmusException;
import com.edu.agh.erasmus.coordinator.university.application.dto.ApplicationApprovalDto;
import com.edu.agh.erasmus.coordinator.university.application.dto.ApplicationUpdateDto;
import com.edu.agh.erasmus.coordinator.university.application.dto.ApplicationsGetDto;
import com.edu.agh.erasmus.coordinator.university.application.dto.ApplicationsSaveDto;
import com.edu.agh.erasmus.coordinator.university.application.dto.ApprovedApplicationsDto;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

import static com.edu.agh.erasmus.coordinator.infrastructure.security.SecurityProperties.AUTH_HEADER;
import static com.edu.agh.erasmus.coordinator.university.utils.Authorities.ADMINISTRATOR_OR_COORDINATOR;
import static com.edu.agh.erasmus.coordinator.university.utils.Authorities.ONLY_ADMINISTRATOR;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/applications/")
public class ApplicationController {

    private final ApplicationService applicationService;

    @Timed
    @PostMapping
    @ApiOperation(value = "Adds new student university application")
    ResponseEntity<ApplicationsGetDto> addUniversityApplication(@RequestBody ApplicationsSaveDto application,
                                                                @RequestHeader(AUTH_HEADER) String token,
                                                                @RequestParam(value = "error", defaultValue = "false") boolean isError,
                                                                @RequestParam(value = "sleep", defaultValue = "0") int sleepTime) throws ErasmusException, InterruptedException {
        if (isError) {
            throw new ErasmusException("Errors are enabled", HttpStatus.INTERNAL_SERVER_ERROR);
        } else if (sleepTime > 0) {
            Thread.sleep(sleepTime);
        }
        return new ResponseEntity<>(applicationService.addApplication(application, token), HttpStatus.CREATED);
    }

    @Timed
    @GetMapping("applier/{applierId}")
    @ApiOperation(value = "Gets university application by applierId")
    ResponseEntity<ApplicationsGetDto> getUniversityApplicationByApplierId(@PathVariable String applierId) {
        return new ResponseEntity<>(applicationService.getUniversityApplicationByApplierId(applierId), HttpStatus.OK);
    }

    @Timed
    @PutMapping("applier_data")
    @ApiOperation(value = "Updates applier data in student university application")
    ResponseEntity<ApplicationsGetDto> updateApplierDataInUniversityApplication(@RequestBody ApplicationUpdateDto application) {
        return new ResponseEntity<>(applicationService.updateApplicationApplierData(application), HttpStatus.OK);
    }

    @Timed
    @PutMapping("offer_data")
    @ApiOperation(value = "Updates offer data in student university application")
    ResponseEntity<ApplicationsGetDto> updateOfferDataInUniversityApplication(@RequestBody ApplicationUpdateDto application) {
        return new ResponseEntity<>(applicationService.updateApplicationOfferData(application), HttpStatus.OK);
    }

    @Timed
    @DeleteMapping("{id}")
    @ApiOperation(value = "Removes student university application")
    ResponseEntity<ResponseEntity> removeUniversityApplication(@PathVariable String id) {
        applicationService.removeApplication(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Timed
    @GetMapping
    @ApiOperation(value = "Gets all university student applications")
    @PreAuthorize(ONLY_ADMINISTRATOR)
    ResponseEntity<Collection<ApplicationsGetDto>> getUniversityApplications() {
        return new ResponseEntity<>(applicationService.getAllApplications(), HttpStatus.OK);
    }

    @Timed
    @GetMapping("{id}")
    @ApiOperation(value = "Gets university application by id")
    @PreAuthorize(ONLY_ADMINISTRATOR)
    ResponseEntity<ApplicationsGetDto> getUniversityApplicationById(@PathVariable String id) {
        return new ResponseEntity<>(applicationService.getUniversityApplicationById(id), HttpStatus.OK);
    }

    @Timed
    @GetMapping("approved")
    @ApiOperation(value = "Gets only approved applications")
    @PreAuthorize(ADMINISTRATOR_OR_COORDINATOR)
    ResponseEntity<Collection<ApprovedApplicationsDto>> getApprovedApplications(@RequestParam(value = "department", required = false) String department,
                                                                                @RequestParam(value = "coordinatorName", required = false) String coordinatorName,
                                                                                @RequestParam(value = "approvedByDwz", required = false) Boolean approvedByDwz) {
        return new ResponseEntity<>(applicationService.getApproved(department, coordinatorName, approvedByDwz), HttpStatus.OK);
    }

    @Timed
    @PutMapping("approve")
    @ApiOperation(value = "Updates dwz approval status in student university application")
    @PreAuthorize(ADMINISTRATOR_OR_COORDINATOR)
    ResponseEntity<ApplicationsGetDto> updateApprovalDwzApplicationStatus(@RequestBody ApplicationApprovalDto approval) {
        applicationService.updateApplicationApprovalStatus(approval);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
