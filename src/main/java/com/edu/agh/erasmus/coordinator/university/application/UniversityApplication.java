package com.edu.agh.erasmus.coordinator.university.application;

import com.edu.agh.erasmus.coordinator.university.offer.Offer;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Wither;
import org.bson.types.ObjectId;

@Data
@Builder
@Wither
public class UniversityApplication {
    private final ObjectId universityOfferId;
    private final Offer offerData;
    private final ApplicationStatus status;
    private final int preference;
    private final String duration;
    private final String semester;
    private final boolean isApplierFromTheSameDepartmentAsOffer;
}

