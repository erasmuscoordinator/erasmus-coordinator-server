package com.edu.agh.erasmus.coordinator.university.state;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StateRepository extends MongoRepository<State, ObjectId> {
}
