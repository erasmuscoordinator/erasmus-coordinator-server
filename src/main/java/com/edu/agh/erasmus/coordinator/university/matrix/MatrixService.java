package com.edu.agh.erasmus.coordinator.university.matrix;

import com.edu.agh.erasmus.coordinator.infrastructure.exception.ErasmusException;
import com.edu.agh.erasmus.coordinator.university.application.ApplicationRepository;
import com.edu.agh.erasmus.coordinator.university.application.ApplicationStatus;
import com.edu.agh.erasmus.coordinator.university.application.UniversityApplication;
import com.edu.agh.erasmus.coordinator.university.application.UniversityApplications;
import com.edu.agh.erasmus.coordinator.university.application.dto.ApplicationApplierDto;
import com.edu.agh.erasmus.coordinator.university.application.dto.ApplicationGetDto;
import com.edu.agh.erasmus.coordinator.university.application.dto.ApplicationsGetDto;
import com.edu.agh.erasmus.coordinator.university.offer.Offer;
import com.edu.agh.erasmus.coordinator.university.offer.OfferService;
import com.edu.agh.erasmus.coordinator.university.user.ApplicationUserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.edu.agh.erasmus.coordinator.university.application.ApplicationStatus.APPROVED;
import static com.edu.agh.erasmus.coordinator.university.application.ApplicationStatus.EXCLUDED;
import static com.edu.agh.erasmus.coordinator.university.application.ApplicationStatus.UNKNOWN;
import static com.edu.agh.erasmus.coordinator.university.utils.ObjectIdConverter.toObjectId;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Service
@RequiredArgsConstructor
class MatrixService {

    private final ApplicationRepository applicationRepository;
    private final OfferService offerService;

    Collection<MatrixDto> updateMatrix(Collection<MatrixDto> matrix, String token) {
        Map<ApplicationUserDto, List<ApplicationGetDto>> userToApplications = getUserToApplications(matrix);
        userToApplications = validateApplications(userToApplications);
        userToApplications.forEach(this::updateApplications);
        return getMatrixForDepartment(token);
    }

    private Map<ApplicationUserDto, List<ApplicationGetDto>> getUserToApplications(Collection<MatrixDto> matrix) {
        return matrix.stream()
                .filter(it -> !it.getApplications().isEmpty())
                .flatMap(it -> it.getApplications().stream())
                .sorted(comparing(it -> it.getApplication().getPreference()))
                .collect(groupingBy(ApplicationApplierDto::getUser,
                        mapping(ApplicationApplierDto::getApplication, toList())
                ));
    }

    private Map<ApplicationUserDto, List<ApplicationGetDto>> validateApplications(Map<ApplicationUserDto, List<ApplicationGetDto>> userToApplications) {
        return userToApplications.entrySet().stream()
                .collect(toMap(Map.Entry::getKey, e -> validateApplicationsEntry(e.getKey(), e.getValue())));
    }

    private List<ApplicationGetDto> validateApplicationsEntry(ApplicationUserDto user, List<ApplicationGetDto> applications) {
        Map<ApplicationStatus, Long> userApplicationsStatus = applications.stream()
                .collect(groupingBy(it -> ApplicationStatus.valueOf(it.getStatus()), Collectors.counting()));
        EnumSet.allOf(ApplicationStatus.class).forEach(it -> userApplicationsStatus.putIfAbsent(it, 0L));

        if (userApplicationsStatus.get(APPROVED) > 1) {
            throw new ErasmusException("Too many approved applications for user: " + user.getId(), BAD_REQUEST);
        }
        if (userApplicationsStatus.get(APPROVED) == 0 && userApplicationsStatus.get(UNKNOWN) == 1) {
            ApplicationGetDto singleApplication = applications.stream().filter(it -> it.getStatus().equals(UNKNOWN.toString())).findFirst().get();
            ApplicationGetDto changedApplication = singleApplication.withStatus(APPROVED.toString());
            applications.set(applications.indexOf(singleApplication), changedApplication);
        }
        return applications;
    }

    private void updateApplications(ApplicationUserDto user, List<ApplicationGetDto> applications) {
        UniversityApplications userApplications = applicationRepository.findOneByApplierId(toObjectId(user.getId()))
                .orElseThrow(() -> new ErasmusException("No application found for user id in matrix: " + user.getId(), NOT_FOUND));
        List<UniversityApplication> universityApplications = applications.stream()
                .map(applicationGetDto -> applicationGetDto.asUniversityApplication(userApplications.getApplierData()))
                .collect(toList());
        UniversityApplications applicationsToSave = userApplications.withApplications(universityApplications);
        applicationRepository.save(applicationsToSave);
    }

    Collection<MatrixDto> getMatrixSuggested(String token) {
        Map<ApplicationUserDto, List<ApplicationGetDto>> userToApplications = getUserToApplications(getMatrixForDepartment(token));
        Map<ApplicationUserDto, List<ApplicationGetDto>> suggestedUserApplications = userToApplications.entrySet().stream()
                .collect(toMap(Map.Entry::getKey, e -> suggestApplicationsEntry(e.getValue())));

        List<ApplicationsGetDto> allApplications = suggestedUserApplications.entrySet().stream()
                .map(it -> ApplicationsGetDto.builder()
                        .applierData(it.getKey())
                        .applications(it.getValue())
                        .build())
                .collect(toList());
        return createMatrix(allApplications, offerService.getOffersForDepartment(token));

    }

    private List<ApplicationGetDto> suggestApplicationsEntry(List<ApplicationGetDto> applications) {
        Optional<ApplicationGetDto> possibleFirstChoice = applications.stream().filter(it -> it.getPreference() == 1).findFirst();
        if (possibleFirstChoice.isPresent()) {
            ApplicationGetDto firstChoice = possibleFirstChoice.get();
            int indexOfFirstChoice = applications.indexOf(firstChoice);
            applications = applications.stream().map(it -> it.withStatus(EXCLUDED.toString())).collect(toList());
            applications.set(indexOfFirstChoice, firstChoice.withStatus(APPROVED.toString()));
        }
        return applications;
    }

    Collection<MatrixDto> getMatrixApproved(String token) {
        return getMatrixForDepartment(token).stream()
                .filter(it -> !it.getApplications().isEmpty())
                .map(it -> new MatrixDto(it.getOffer(), getApprovedApplications(it)))
                .filter(it -> !it.getApplications().isEmpty())
                .collect(toList());
    }

    private List<ApplicationApplierDto> getApprovedApplications(MatrixDto matrix) {
        return matrix.getApplications().stream()
                .filter(it -> it.getApplication().getStatus().equals(APPROVED.toString()))
                .collect(toList());
    }

    Collection<MatrixDto> getMatrixForDepartment(String token) {
        List<ApplicationsGetDto> applications = getAllApplications();
        List<Offer> offers = offerService.getOffersForDepartment(token);
        return createMatrix(applications, offers);
    }

    Collection<MatrixDto> getMatrixForCoordinator(String coordinatorName, String token) {
        List<ApplicationsGetDto> applications = getAllApplications();
        List<Offer> offers = offerService.getOffersForCoordinator(coordinatorName, token);
        return createMatrix(applications, offers);
    }

    private List<ApplicationsGetDto> getAllApplications() {
        return applicationRepository.findAll().stream()
                .map(ApplicationsGetDto::of)
                .collect(toList());
    }

    private Collection<MatrixDto> createMatrix(List<ApplicationsGetDto> applications, List<Offer> offers) {
        List<ApplicationGetDto> allUserApplications = applications.stream().flatMap(it -> it.getApplications().stream()).collect(toList());
        Set<String> applicationsOfferIds = allUserApplications.stream().map(it -> it.getOfferData().getId()).collect(toSet());

        return offers.stream()
                .map(it -> new MatrixDto(it, getApplications(it.getId(), applications, applicationsOfferIds)))
                .collect(toList());
    }

    private List<ApplicationApplierDto> getApplications(String offerId, List<ApplicationsGetDto> applications, Set<String> applicationsOfferIds) {
        if (applicationsOfferIds.contains(offerId)) {
            return applications.stream()
                    .map(it -> matchOffer(it, offerId))
                    .filter(Objects::nonNull)
                    .sorted(comparing(it -> it.getApplication().getPreference()))
                    .collect(toList());
        }
        return Collections.emptyList();
    }

    private ApplicationApplierDto matchOffer(ApplicationsGetDto userApplications, String offerId) {
        ApplicationGetDto application = findOffer(userApplications, offerId);
        if (application != null) {
            ApplicationUserDto applier = userApplications.getApplierData();
            return new ApplicationApplierDto(application, applier);
        } else return null;
    }

    private ApplicationGetDto findOffer(ApplicationsGetDto userApplications, String offerId) {
        return userApplications.getApplications().stream()
                .filter(it -> it.getOfferData().getId().equals(offerId))
                .findFirst()
                .orElse(null);
    }
}
