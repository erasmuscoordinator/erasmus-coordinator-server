package com.edu.agh.erasmus.coordinator.infrastructure.exception;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@Slf4j
@RequiredArgsConstructor
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ErasmusException.class})
    public ResponseEntity<ErasmusExceptionDto> handleErasmusException(ErasmusException erasmusException) {
        logger.error("Erasmus Exception", erasmusException);
        return new ResponseEntity<>(ErasmusExceptionDto.of(erasmusException), erasmusException.getStatus());
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<?> handleOtherException(Exception ex) throws Exception {
        if (ex instanceof ErasmusException) {
            return handleErasmusException((ErasmusException) ex);
        } else if (ex instanceof AccessDeniedException) {
            throw ex;
        }
        logger.error("Error", ex);
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
