package com.edu.agh.erasmus.coordinator.university.application.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
@Builder
public class ApplicationApprovalDto {
    @NotNull private final Set<String> ids;
    private final boolean approvedByDwz;
}
