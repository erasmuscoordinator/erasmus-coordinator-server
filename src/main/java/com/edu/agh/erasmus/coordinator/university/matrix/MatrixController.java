package com.edu.agh.erasmus.coordinator.university.matrix;

import com.codahale.metrics.annotation.Timed;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

import static com.edu.agh.erasmus.coordinator.infrastructure.security.SecurityProperties.AUTH_HEADER;
import static com.edu.agh.erasmus.coordinator.university.utils.Authorities.ADMINISTRATOR_OR_COORDINATOR;
import static com.edu.agh.erasmus.coordinator.university.utils.Authorities.ONLY_ADMINISTRATOR;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/matrix/")
public class MatrixController {

    private final MatrixService matrixService;

    @Timed
    @GetMapping
    @ApiOperation(value = "Gets matrix")
    @PreAuthorize(ONLY_ADMINISTRATOR)
    ResponseEntity<Collection<MatrixDto>> getMatrix(@RequestHeader(AUTH_HEADER) String token) {
        return new ResponseEntity<>(matrixService.getMatrixForDepartment(token), HttpStatus.OK);
    }

    @Timed
    @GetMapping("coordinator/{coordinatorName}")
    @ApiOperation(value = "Gets matrix for coordinator")
    @PreAuthorize(ADMINISTRATOR_OR_COORDINATOR)
    ResponseEntity<Collection<MatrixDto>> getMatrixForCoordinator(@PathVariable String coordinatorName,
                                                                  @RequestHeader(AUTH_HEADER) String token) {
        return new ResponseEntity<>(matrixService.getMatrixForCoordinator(coordinatorName, token), HttpStatus.OK);
    }

    @Timed
    @GetMapping("approved")
    @ApiOperation(value = "Gets matrix with only approved applications")
    @PreAuthorize(ADMINISTRATOR_OR_COORDINATOR)
    ResponseEntity<Collection<MatrixDto>> getMatrixApproved(@RequestHeader(AUTH_HEADER) String token) {
        return new ResponseEntity<>(matrixService.getMatrixApproved(token), HttpStatus.OK);
    }

    @Timed
    @GetMapping("suggested")
    @ApiOperation(value = "Gets matrix with only suggested approvals")
    @PreAuthorize(ONLY_ADMINISTRATOR)
    ResponseEntity<Collection<MatrixDto>> getMatrixSuggested(@RequestHeader(AUTH_HEADER) String token) {
        return new ResponseEntity<>(matrixService.getMatrixSuggested(token), HttpStatus.OK);
    }

    @Timed
    @PutMapping
    @ApiOperation(value = "Updates applications based on matrix")
    @PreAuthorize(ONLY_ADMINISTRATOR)
    ResponseEntity<Collection<MatrixDto>> updateMatrix(@RequestBody Collection<MatrixDto> matrix,
                                                       @RequestHeader(AUTH_HEADER) String token) {
        return new ResponseEntity<>(matrixService.updateMatrix(matrix, token), HttpStatus.OK);
    }
}
