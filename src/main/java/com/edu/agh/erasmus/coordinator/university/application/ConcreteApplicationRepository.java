package com.edu.agh.erasmus.coordinator.university.application;

import com.codahale.metrics.annotation.Timed;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class ConcreteApplicationRepository {

    private final ApplicationRepository applicationRepository;

    @Timed
    UniversityApplications save(UniversityApplications applications) {
        return applicationRepository.save(applications);
    }
}
