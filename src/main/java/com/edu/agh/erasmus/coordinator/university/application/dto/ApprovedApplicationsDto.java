package com.edu.agh.erasmus.coordinator.university.application.dto;

import com.edu.agh.erasmus.coordinator.university.user.ApplicationUserDto;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class ApprovedApplicationsDto {
    private final String userApplicationsId;
    private final ApplicationGetDto application;
    private final ApplicationUserDto applierData;
}
