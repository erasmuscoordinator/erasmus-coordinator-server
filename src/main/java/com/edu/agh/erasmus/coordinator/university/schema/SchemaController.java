package com.edu.agh.erasmus.coordinator.university.schema;

import com.edu.agh.erasmus.coordinator.university.schema.dto.SchemaGetDto;
import com.edu.agh.erasmus.coordinator.university.schema.dto.SchemaSaveDto;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

import static com.edu.agh.erasmus.coordinator.university.utils.Authorities.ONLY_ADMINISTRATOR;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/schema/")
public class SchemaController {
    private final SchemaService schemaService;
    private final SchemaRepository schemaRepository;

    @GetMapping("current")
    @ApiOperation(value = "Get current agreement schema")
    ResponseEntity<SchemaGetDto> getLatestSchema() {
        if (schemaRepository.count() == 0) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(schemaService.getLatestSchema(), HttpStatus.OK);
    }

    @PostMapping("current")
    @ApiOperation(value = "Create new agreement schema that becomes current one")
    @PreAuthorize(ONLY_ADMINISTRATOR)
    ResponseEntity<SchemaGetDto> createSchema(@RequestBody SchemaSaveDto schema) {
        return new ResponseEntity<>(schemaService.addSchema(schema), HttpStatus.OK);
    }

    @GetMapping("historical")
    @ApiOperation(value = "Get historical agreements schemas without current one")
    @PreAuthorize(ONLY_ADMINISTRATOR)
    ResponseEntity<Collection<SchemaGetDto>> getHistoricalSchemas() {
        if (schemaRepository.count() == 0) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(schemaService.getHistoricalSchemas(), HttpStatus.OK);
    }

}
