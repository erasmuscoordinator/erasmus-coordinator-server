package com.edu.agh.erasmus.coordinator.university.utils;

import com.edu.agh.erasmus.coordinator.infrastructure.exception.ErasmusException;
import org.bson.types.ObjectId;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

public final class ObjectIdConverter {
    public static ObjectId toObjectId(String id) {
        ObjectId objectId;
        try {
            objectId = new ObjectId(id);
        } catch (IllegalArgumentException e) {
            throw new ErasmusException("MALFORMED OBJECT ID: " + id, BAD_REQUEST);
        }
        return objectId;
    }
}
