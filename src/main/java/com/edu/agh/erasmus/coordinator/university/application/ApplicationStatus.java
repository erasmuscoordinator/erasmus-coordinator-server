package com.edu.agh.erasmus.coordinator.university.application;

public enum ApplicationStatus{
    UNKNOWN, APPROVED, EXCLUDED
}
