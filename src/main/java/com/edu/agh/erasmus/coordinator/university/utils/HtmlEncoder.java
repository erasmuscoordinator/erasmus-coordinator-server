package com.edu.agh.erasmus.coordinator.university.utils;

import org.owasp.encoder.Encode;

public class HtmlEncoder {

    public static String htmlEncode(String input) {
        if (input == null) {
            return null;
        }
        return Encode.forHtml(input);
    }
}
