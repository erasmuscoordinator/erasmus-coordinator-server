package com.edu.agh.erasmus.coordinator.university.schema;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface SchemaRepository extends MongoRepository<Schema, ObjectId> {
    Schema findTopByOrderByModifiedDesc();
    List<Schema> findAllByOrderByModifiedDesc();
}
