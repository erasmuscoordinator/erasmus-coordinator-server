package com.edu.agh.erasmus.coordinator.university.schema;

import com.edu.agh.erasmus.coordinator.university.schema.dto.SchemaGetDto;
import com.edu.agh.erasmus.coordinator.university.schema.dto.SchemaSaveDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class SchemaService {
    private final SchemaRepository schemaRepository;

    SchemaGetDto getLatestSchema() {
        return SchemaGetDto.of(schemaRepository.findTopByOrderByModifiedDesc());
    }

    List<SchemaGetDto> getHistoricalSchemas() {
        return schemaRepository.findAllByOrderByModifiedDesc().stream()
                .map(SchemaGetDto::of)
                .collect(Collectors.toList());
    }

    SchemaGetDto addSchema(SchemaSaveDto schema) {
        Schema schemaToAdd = schema.asSchema();
        return SchemaGetDto.of(schemaRepository.save(schemaToAdd));
    }
}
