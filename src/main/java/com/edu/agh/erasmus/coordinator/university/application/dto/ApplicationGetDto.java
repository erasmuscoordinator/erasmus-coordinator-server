package com.edu.agh.erasmus.coordinator.university.application.dto;

import com.edu.agh.erasmus.coordinator.university.application.ApplicationStatus;
import com.edu.agh.erasmus.coordinator.university.application.UniversityApplication;
import com.edu.agh.erasmus.coordinator.university.offer.Offer;
import com.edu.agh.erasmus.coordinator.university.user.User;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Wither;

import static com.edu.agh.erasmus.coordinator.university.utils.DepartmentsCheckUtil.checkDepartmentsEquality;
import static com.edu.agh.erasmus.coordinator.university.utils.HtmlEncoder.htmlEncode;
import static com.edu.agh.erasmus.coordinator.university.utils.ObjectIdConverter.toObjectId;

@Data
@Builder
@Wither
public class ApplicationGetDto {
    private final String universityOfferId;
    private final Offer offerData;
    private final String status;
    private final int preference;
    private final String duration;
    private final String semester;
    private final boolean isApplierFromTheSameDepartmentAsOffer;

    public static ApplicationGetDto of(UniversityApplication application) {
        return builder()
                .universityOfferId(application.getUniversityOfferId().toHexString())
                .offerData(application.getOfferData())
                .status(String.valueOf(application.getStatus()))
                .preference(application.getPreference())
                .duration(htmlEncode(application.getDuration()))
                .semester(htmlEncode(application.getSemester()))
                .isApplierFromTheSameDepartmentAsOffer(application.isApplierFromTheSameDepartmentAsOffer())
                .build();
    }

    public UniversityApplication asUniversityApplication(User user) {
        return UniversityApplication.builder()
                .universityOfferId(toObjectId(universityOfferId))
                .offerData(offerData)
                .status(ApplicationStatus.valueOf(status))
                .preference(preference)
                .duration(duration)
                .isApplierFromTheSameDepartmentAsOffer(checkDepartmentsEquality(offerData.getDepartment(), user.getDepartment()))
                .build();
    }
}
