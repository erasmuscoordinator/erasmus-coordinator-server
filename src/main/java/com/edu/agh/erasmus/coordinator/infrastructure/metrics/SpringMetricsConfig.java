package com.edu.agh.erasmus.coordinator.infrastructure.metrics;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.jvm.GarbageCollectorMetricSet;
import com.codahale.metrics.jvm.MemoryUsageGaugeSet;
import com.codahale.metrics.jvm.ThreadStatesGaugeSet;
import com.izettle.metrics.dw.InfluxDbReporterFactory;
import com.ryantenney.metrics.spring.config.annotation.EnableMetrics;
import com.ryantenney.metrics.spring.config.annotation.MetricsConfigurerAdapter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import static java.util.concurrent.TimeUnit.MINUTES;

@Configuration
@EnableMetrics
public class SpringMetricsConfig extends MetricsConfigurerAdapter {

    private final String influxUser;
    private final String influxPassword;
    private final String influxHost;
    private final String influxDatabase;

    public SpringMetricsConfig(
            @Value("${influx.username}") String influxUser,
            @Value("${influx.password}") String influxPassword,
            @Value("${influx.hostname}") String influxHost,
            @Value("${influx.database}") String influxDatabase) {
        this.influxUser = influxUser;
        this.influxPassword = influxPassword;
        this.influxHost = influxHost;
        this.influxDatabase = influxDatabase;
    }

    @Override
    public void configureReporters(MetricRegistry metricRegistry) {
        InfluxDbReporterFactory influxDbReporterFactory = new InfluxDbReporterFactory();
        influxDbReporterFactory.setHost(influxHost);
        influxDbReporterFactory.setDatabase(influxDatabase);
        influxDbReporterFactory.setAuth(influxUser + ":" + influxPassword);
        influxDbReporterFactory.build(metricRegistry).start(1, MINUTES);
        metricRegistry.register("gc", new GarbageCollectorMetricSet());
        metricRegistry.register("memory", new MemoryUsageGaugeSet());
        metricRegistry.register("threads", new ThreadStatesGaugeSet());

        // registerReporter allows the MetricsConfigurerAdapter to
        // shut down the reporter when the Spring context is closed
        registerReporter(ConsoleReporter
                .forRegistry(metricRegistry)
                .build())
                .start(1, MINUTES);
    }

}