package com.edu.agh.erasmus.coordinator.university.schema.dto;

import com.edu.agh.erasmus.coordinator.university.schema.Schema;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class SchemaGetDto {
    private final String schemaId;
    private final String modifiedByUsername;
    private final String content;
    private final String modificationReason;
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonFormat(pattern="dd/MM/yyyy hh:mm:ss")
    private final LocalDateTime modified;

    public static SchemaGetDto of(Schema schema) {
        return builder()
                .schemaId(schema.getSchemaId().toHexString())
                .modifiedByUsername(schema.getModifiedByUsername())
                .content(schema.getContent())
                .modified(schema.getModified())
                .modificationReason(schema.getModificationReason())
                .build();
    }
}
